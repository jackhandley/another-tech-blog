import React, { useEffect, useState } from "react";
import { config } from "../util/config";

export default function Comments({ issue }) {
  const [notes, setNotes] = useState()

  useEffect(() => {
    fetch(`${config.gitlabBaseUrl}/api/v4/projects/${config.projectId}/issues/${issue.iid}/notes`)
      .then(response => response.json())
      .then(setNotes)
  }, [issue])

  if (notes === undefined) {
    return <p>Loading...</p>
  }

  return (
    <div>
      {JSON.stringify(notes)}
    </div>
  )
}