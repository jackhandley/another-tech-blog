function PublishedDate({dateString}) {
  const date = new Date(Date.parse(dateString))
  const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  return  dateString ? <span>{date.toLocaleDateString(undefined, options)}</span> : <></>
}

export {
  PublishedDate
}