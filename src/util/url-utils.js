function titleToUrl(title) {
  return title.replaceAll(" ", "_")
}

export {
  titleToUrl
}