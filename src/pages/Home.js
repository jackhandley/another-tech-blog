import StaticPage from "../components/StaticPage";

export default function Home() {
  return <StaticPage urlString={"index.md"} />
}